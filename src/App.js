import './App.css';
import Characters from './components/Characters';
import imageRickMorty from './img/rick-morty.png'
import { useState } from 'react';


function App() {

  const [characters, setCharacters] = useState(null);
  const reqApi = async () =>{
    const api = await fetch('https://rickandmortyapi.com/api/character');
    const caracterApi = await api.json();
    setCharacters(caracterApi.results);    
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1 className='title'> rick & morty</h1>
        {
          characters ? 
          (
            <Characters characters = {characters} setCharacters = {setCharacters} ></Characters>
          )
          :
          (
            <>
            <img src={imageRickMorty} alt='rick'className='img-home'></img>
            <button className='btn-search' onClick={reqApi} >Buscar personaje</button>
            </>
          )
        }                
      </header>
    </div>
  );
}

export default App;
